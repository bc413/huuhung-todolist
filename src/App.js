import logo from "./logo.svg";
import "./App.css";
import DemoTheme from "./JSS_StyledComponent/Theme/DemoTheme";
import ToDoList from "./JSS_StyledComponent/BaiTapStyledComponent/ToDoList";

function App() {
  return (
    <div className="App text-left">
      {/* <DemoJSS />
      <DemoTheme /> */}
      <ToDoList />
    </div>
  );
}

export default App;

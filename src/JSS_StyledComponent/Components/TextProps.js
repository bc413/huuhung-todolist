import styled from "styled-components";

export let Input = styled.input`
  color: ${(props) => props.color || "blue"};
  font-size: 100px;
`;

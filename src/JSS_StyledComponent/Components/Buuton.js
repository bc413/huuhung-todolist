import styled from "styled-components";

export const Button = styled.button`
  color: ${(props) => (props.red ? "black" : "pink")};
  background-color: yellow;
  cursor: pointer;
  &:hover {
    color: blue;
    transition: all 0.5s;
  }
  &.Button_Style {
    font-size: ${(props) => (props.fonSize ? "100px" : "0px")};
  }
`;

export const SmallButton = styled(Button)`
  color: ${(props) => (props.mauXanh ? "red" : "white")};
  background-color: black;
`;

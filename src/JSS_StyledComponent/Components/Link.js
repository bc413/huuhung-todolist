import styled from "styled-components";
import React from "react";

export let Link = ({ className, children, ...resProps }) => (
  <a className={className} href="">
    {children}
  </a>
);

export let StyleLink = styled(Link)`
  color: red;
  font-size: 30px;
  background-color: black;
`;

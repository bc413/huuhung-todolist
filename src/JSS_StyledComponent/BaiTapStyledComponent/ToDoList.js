import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "../Theme/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../Theme/ToDoListPrimaryTheme";

import { Container } from "../ComponentsToDoList/Container";
import { Dropdown } from "../ComponentsToDoList/Dropdown";
import { TextField } from "../ComponentsToDoList/TextField";
import { Button } from "../ComponentsToDoList/Button";
import { Table, Th, Thead, Tr } from "../ComponentsToDoList/Table";
import { Heading1, Heading3, Heading4 } from "../ComponentsToDoList/Heading";

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
} from "../../Redux/actions/ToDoListActions";
import { arrTheme } from "../Theme/ThemManager";
import { edit_task } from "../../Redux/types/ToDoList";

class ToDoList extends Component {
  state = {
    taskName: "",
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((item, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{item.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch({
                    type: edit_task,
                    task: item,
                  });
                }}
              >
                <i class="fa-solid fa-pen-to-square"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(item.id));
                }}
                className=" mx-2"
              >
                <i class="fa-solid fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(item.id));
                }}
              >
                <i class="fa-solid fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((item, index) => {
        return (
          <Tr key={index}>
            <Th>{item.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(item.id));
                }}
              >
                <i class="fa-solid fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTheme = () => {
    return arrTheme.map((item, index) => {
      return (
        <option key={index} value={item.id}>
          {item.name}
        </option>
      );
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              this.props.dispatch(changeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading3>TO DO LIST</Heading3>
          <TextField
            value={this.props.taskEdit.taskName}
            onChange={(e) => {
              this.setState(
                {
                  taskName: e.target.value,
                },
                () => {
                  console.log(this.state);
                }
              );
            }}
            name="taskName"
            className="w-50"
            label="TaskName"
          ></TextField>
          <Button
            onClick={() => {
              let { taskName } = this.state;
              let newtask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              console.log(newtask);
              this.props.dispatch(addTaskAction(newtask));
            }}
            className="ml-2"
          >
            <i class="fa-solid fa-plus"></i> Add Task
          </Button>
          <Button className="ml-2">
            <i class="fa-solid fa-upload"></i> Update Task
          </Button>
          <hr />
          <Heading4>Tas to do</Heading4>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading4>Tas completed</Heading4>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.todolistReducer.themeToDoList,
    taskList: state.todolistReducer.taskList,
    taskEdit: state.todolistReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);

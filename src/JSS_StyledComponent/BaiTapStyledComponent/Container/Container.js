import styled from "styled-components";

export let Container = styled.div`
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  border: 1px solid #000;
  background-color: ${(props) => props.theme.bgColor};
  color: ${(props) => props.theme.color};
`;

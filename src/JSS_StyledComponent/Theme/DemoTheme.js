import React, { useState } from "react";
import styled, { ThemeProvider } from "styled-components";

export default function DemoTheme(propsComponent) {
  let DrakTheme = {
    color: "#fff",
    backgroundColor: "Black",
    fonSize: "30px",
    padding: "30px",
  };
  let LightTheme = {
    color: "red",
    backgroundColor: "blue",
    fonSize: "30px",
    padding: "30px",
    colorSpan: "yellow",
  };

  let [state, setState] = useState(DrakTheme);

  let handleOnchang = (e) => {
    e.preventDefault();
    if (e.target.value == "2") {
      setState(LightTheme);
    } else if (e.target.value === 1 || e.target.value == "0") {
      setState(DrakTheme);
    }
  };

  const DivStyle = styled.div`
    color: ${(props) => props.theme.color};
    background-color: ${(props) => props.theme.backgroundColor};
    padding: ${(props) => props.theme.padding};
  `;
  const Span = styled.span`
    color: ${(props) => props.theme.colorSpan};
    padding: 20px;
  `;

  return (
    <ThemeProvider theme={state}>
      <div>
        <h2>DemoTheme</h2>
        <DivStyle>Tôi là nguyễn Hữu Hùng Mang thông thạo 10</DivStyle>
      </div>
      <select onChange={handleOnchang}>
        <option value="0">Lựa chọn</option>
        <option value="1">DrakTheme</option>
        <option value="2">LightTheme</option>
      </select>

      <Span>Huu Hung</Span>
    </ThemeProvider>
  );
}

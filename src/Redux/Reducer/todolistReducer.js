import { arrTheme } from "../../JSS_StyledComponent/Theme/ThemManager";
import { ToDoListDarkTheme } from "../../JSS_StyledComponent/Theme/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../JSS_StyledComponent/Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../../JSS_StyledComponent/Theme/ToDoListPrimaryTheme";
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
} from "../types/ToDoList";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: "task-1", taskName: "task 1", done: true },
    { id: "task-2", taskName: "task 2", done: false },
    { id: "task-3", taskName: "task 3", done: true },
    { id: "task-4", taskName: "task 4", done: false },
  ],
  taskEdit: { id: "task-1", taskName: "task 1", done: false },
};

let todolistReducer = (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      console.log(action.newTask);
      if (action.newTask.taskName.trim() === "") {
        alert("task name is require");
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((item) => {
        return item.taskName == action.newTask.taskName;
      });
      if (index !== -1) {
        alert("task name already exists");
        return { ...state };
      }

      taskListUpdate.push(action.newTask);
      return { ...state, taskList: taskListUpdate };
    }
    case change_theme: {
      let theme = arrTheme.find((theme) => theme.id == action.themId);
      if (theme) {
        return { ...state, themeToDoList: theme.theme };
      }
      return { ...state };
    }
    case done_task: {
      let taskListUpadate = [...state.taskList];
      let index = taskListUpadate.findIndex((item) => {
        return item.id === action.taskId;
      });
      if (index !== -1) {
        taskListUpadate[index].done = true;
      }
      return { ...state, taskList: taskListUpadate };

      console.log(index);
      return { ...state };
    }
    case delete_task: {
      let taskListUpadate = [...state.taskList];
      taskListUpadate = taskListUpadate.filter((item) => {
        return item.id !== action.taskId;
      });

      return { ...state, taskList: taskListUpadate };
    }
    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    default:
      return state;
  }
};

export default todolistReducer;
